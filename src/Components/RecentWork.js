import React from "react";
import Slide from "react-reveal/Slide";

const RecentWork = () => {
  return (
    <div className="recent-work" id="work">
      <div className="main-slide">
        <Slide right big>
          <div className="heading">A Few Examples Of Our Passion</div>
          <hr height="3px" />
          <div className="description-text">
            <p>
              Check this out and see it for yourself what MDSol Technologies can
              deliver to you. These eye-catching designs are apples for so many
              eyes around the world, leveraging unexpected incline in ROI.
            </p>
          </div>
        </Slide>
      </div>
    </div>
  );
};

export default RecentWork;
