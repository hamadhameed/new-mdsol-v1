import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Navbar } from "react-bootstrap";
import { Container } from "react-bootstrap";
import { Nav } from "react-bootstrap";
import logo from "../assets/images/logo.png";
import logoWhite from "../assets/images/white-logo.png";
import icon from "../assets/images/down-arrow.png";
import { useState, useEffect } from "react";
import Zoom from "react-reveal/Zoom";
import Fade from "react-reveal/Fade";
import close from "../assets/images/close-icon.png";
const Header = () => {
  const [colorChange, setColorchange] = useState(false);
  const [isOpen, setIsOpen] = useState(false);
  const [id, setId] = useState('');
  const mobileMenu = (val) => {
    setIsOpen(false);
    setId(val);
  };
  const changeNavbarColor = () => {
    if (window.scrollY >= 80) {
      setColorchange(true);
    } else {
      setColorchange(false);
    }
  };
  useEffect(() => {
    window.addEventListener("scroll", changeNavbarColor);

    return () => window.removeEventListener("scroll", changeNavbarColor);
  }, []);
  return (
    <div className="main-header">
      {/* Banner-Image-Start */}
      {/* Banner-Image-Start */}
      <div className="banner-img" id="home">
        {/* Navigation-Bar-Start */}
        {/* Navigation-Bar-Start */}
        <Navbar
          collapseOnSelect
          className={
            colorChange ? "colorChange" : "navbar"
          }
          expand="false"
          fixed="top"
        >
          <Container fluid>
            <Navbar.Brand className="navbar-brand" href="#home">
              <img src={logo} className="logo-img" alt="" />

            </Navbar.Brand>
            <Navbar.Toggle
              className="border-0 navbar-toggle-button"
              aria-controls="responsive-navb
              ar-nav"
              onClick={() => setIsOpen(true)}
            >
              <i className="fas fa-bars icon"></i>
            </Navbar.Toggle>
          </Container>
          <div id="responsive-navbar-nav" className={`collapse-div ${isOpen ? 'open' : 'close'}`} >
            <div className="close-icon">
              <img src={close} alt="" onClick={() => setIsOpen(false)} />
            </div>
            <a className="img-logo" href="#home" onClick={() => mobileMenu('')}>
              <img src={logoWhite} alt="" />
            </a>
            <Nav className="me-auto">
              <Nav.Link
                className={`nav-links-background ${id === "home" ? "active" : ""}`}
                onClick={() => mobileMenu('home')}
                href="#home"
              >
                Home
              </Nav.Link>
              <Nav.Link
                className={`nav-links-background ${id === "aboutUs" ? "active" : ""}`}
                onClick={() => mobileMenu('aboutUs')}
                href="#aboutUs"
              >
                About Us
              </Nav.Link>
              <Nav.Link
                className={`nav-links-background ${id === "services" ? "active" : ""}`}
                onClick={() => mobileMenu('services')}
                href="#services"
              >
                Our Services
              </Nav.Link>
              <Nav.Link
                className={`nav-links-background ${id === "work" ? "active" : ""}`}
                onClick={() => mobileMenu('work')}
                href="#work"
              >
                Our Work
              </Nav.Link>
              <Nav.Link
                className={`nav-links-background ${id === "clients" ? "active" : ""}`}
                onClick={() => mobileMenu('clients')}
                href="#clients"
              >
                Our Clients
              </Nav.Link>
              <Nav.Link
                className={`nav-links-background ${id === "team" ? "active" : ""}`}
                onClick={() => mobileMenu('team')}
                href="#team"
              >
                Our Team
              </Nav.Link>
              <Nav.Link
                className={`nav-links-background ${id === "contact" ? "active" : ""}`}
                onClick={() => mobileMenu('contact')}
                href="#contact"
              >
                Contact
              </Nav.Link>
            </Nav>
            <div className="social-media-icons">
              <div className="facebook flex-item">
                <a href="https://www.facebook.com/MDSolTechnologies" rel="noreferrer" target="_blank">
                  <svg className="face" xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" viewBox="0 0 24 24"><path d="M15.12,5.32H17V2.14A26.11,26.11,0,0,0,14.26,2C11.54,2,9.68,3.66,9.68,6.7V9.32H6.61v3.56H9.68V22h3.68V12.88h3.06l.46-3.56H13.36V7.05C13.36,6,13.64,5.32,15.12,5.32Z" /></svg>
                </a>
              </div>
              <div className="twitter flex-item">
                <a href="https://www.linkedin.com/company/mdsol-technologies/" rel="noreferrer" target="_blank">
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 5 1036 990"><path d="M0 120c0-33.334 11.667-60.834 35-82.5C58.333 15.833 88.667 5 126 5c36.667 0 66.333 10.666 89 32 23.333 22 35 50.666 35 86 0 32-11.333 58.666-34 80-23.333 22-54 33-92 33h-1c-36.667 0-66.333-11-89-33S0 153.333 0 120zm13 875V327h222v668H13zm345 0h222V622c0-23.334 2.667-41.334 8-54 9.333-22.667 23.5-41.834 42.5-57.5 19-15.667 42.833-23.5 71.5-23.5 74.667 0 112 50.333 112 151v357h222V612c0-98.667-23.333-173.5-70-224.5S857.667 311 781 311c-86 0-153 37-201 111v2h-1l1-2v-95H358c1.333 21.333 2 87.666 2 199 0 111.333-.667 267.666-2 469z" /></svg>
                </a>
              </div>
            </div>
          </div>

        </Navbar>
        {/* Nav-Bar-Ends */}
        {/* Nav-Bar-Ends */}
        {/* Banner-Text-Start */}
        {/* Banner-Text-Start */}

        <div className="wrapper">
          {/* left-section start */}
          {/* left-section start */}
          <Zoom duration={1400}>
            <div className="left-text-section">
              <div className="border-top-text">
                <Fade left delay={1000}>
                  WEB & MOBILE DEVELOPMENT
                </Fade>
              </div>
              <div className="left-heading">
                <h1>WE</h1>
              </div>
              <div className="border-bottom-text">
                <Fade right delay={1000}>
                  MARKETING & UX/UI DESIGN
                </Fade>
              </div>
            </div>
          </Zoom>
          {/* left-section ends */}
          {/* left-section ends */}
          {/* right-section start */}
          {/* right-section start */}
          <Zoom duration={1400}>
            <div className="right-text-section">
              <div className="right-heading">
                <h1>
                  LAUNCH <br /> BRANDS
                </h1>
              </div>
            </div>
          </Zoom>
          {/* right-section ends */}
          {/* right-section ends */}
        </div>

        {/* Banner-Text-ENDS */}
        {/* Banner-Text-ENDS */}
        {/* Banner-ICON-STARTS*/}
        {/* Banner-ICON-STARTS */}
        {/* <Bounce bottom duration={2000}> */}
        <div className="down-arrow-icon text-center">
          <a href="#aboutUs">
            <img src={icon} alt="" height="62" width="62" />
          </a>
        </div>
        {/* </Bounce> */}

        {/* Banner-ICON-ENDS*/}
        {/* Banner-ICON-ENDS */}
      </div>
      {/* Banner-Image-Ends */}
      {/* Banner-Image-Ends */}
    </div>
  );
};
export default Header;
