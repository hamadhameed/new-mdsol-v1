import React from "react";
import { Col, Row } from "react-bootstrap";
import { Container } from "react-bootstrap";
import Slide from "react-reveal/Slide";
const WhoWeAre = () => {
  return (
    <div className="whoweare-wrapper" id="aboutUs">
      <Container fluid className="Container-wrapper">
        <Row>
          {/* Image Column Starts */}
          <Col lg={6} md={12} sm={12} xs={12} className="image-section"></Col>
          {/* Image Column Ends */}
          {/* Text Column Starts */}

          <Col lg={6} md={12} sm={12} xs={12} className="text-section">
            <Slide right>
              <div className="heading">
                <h1>
                  PASSIONATE COMPANY
                </h1>
              </div>
              <div className="subHeading">
                <h6>In Web and Mobile App</h6>
                <hr color="#77BF43" />
              </div>
              <div className="text-description">
                <p>
                  MDSol Tech has everything you need to make your business successful. Whether that be the responsive websites, the web, and mobile apps or the digital presence. We are passionate about your success. And we turn the tides in your favor for fabulous results. The extensive website development solutions enable you to catch up with the highest profits. And with our mobile app solutions, you turn your every lead into a staunch customer.
                </p>

              </div>
            </Slide>
          </Col>

          {/* Text Column Ends */}
        </Row>
      </Container>
    </div>
  );
};

export default WhoWeAre;
