import React, { useState } from "react";
import { Form, Button } from "react-bootstrap";

const FormSection = () => {
  const [formData, setFormData] = useState('');
  const [isSuccess, setIsSuccess] = useState(false);
  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    if (formData && formData.email && formData.name && formData.message) {
      console.log(formData);
      send();
    }
  };
  const send = () => {
    const smtpjs = window.Email;
    smtpjs
      .send({
        SecureToken: "27620ab6-a70f-4d34-bd43-14cea878c7c6",
        To: "info@mdsoltech.com",
        From: formData.name + "<hheview@gmail.com>",
        Subject: "Get In Touch",
        Body:
          "Name: " +
          formData.name +
          "<br/>" +
          "Email: " +
          formData.email +
          "<br/>" +
          "Message: " +
          formData.message +
          "<br/>",
      })
      .then((result) => {
        console.log(result);
        if (result === "OK") {
          setFormData({ ...formData, name: "", email: "", message: "" });
          setIsSuccess(true);
        }
      });
  };
  return (
    <div className="form-section-wrapper">
      <div className="heading">get in touch</div>
      <hr />
      <div className="form-section">
        <Form onSubmit={handleSubmit} autoComplete="off">
          <Form.Group
            className="mb-4"
            controlId="formBasicText"
            autoComplete="off"
          >
            <Form.Control
              required
              className="input-style"
              type="text"
              placeholder="Name*"
              name="name"
              value={formData && formData.name && formData.name}
              onChange={handleChange}
              autoComplete="off"
            />
          </Form.Group>
          <Form.Group className="mb-4" controlId="formBasicEmail">
            <Form.Control
              required
              className="input-style"
              type="email"
              placeholder="Email Address*"
              name="email"
              value={formData && formData.email && formData.email}
              onChange={handleChange}
              autoComplete="off"
            />
          </Form.Group>
          <Form.Control
            as="textarea"
            className="mb-4 input-style"
            placeholder="Your Message"
            style={{ height: "100px" }}
            name="message"
            value={formData && formData.message && formData.message}
            onChange={handleChange}
          />
          {isSuccess && (
            <div className="success">
              Thank you for contacting us. we will back to you shortly!
            </div>
          )}
          <div className="submit-button">
            <Button type="submit">Submit</Button>
          </div>
        </Form>
      </div>
    </div>
  );
};

export default FormSection;
