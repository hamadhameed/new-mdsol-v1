import React from "react";
import { Row, Col, Container } from "react-bootstrap";
import webImg from "../assets/images/about/1.jpg";
import mobImg from "../assets/images/about/2.jpg";
import netImg from "../assets/images/about/3.jpg";
import four from "../assets/images/about/4.jpg";
import five from "../assets/images/about/5.jpg";
import six from "../assets/images/about/6.jpg";
import Zoom from "react-reveal/Zoom";

const Services = () => {
  return (
    <div className="main-wrapper" id="services">
      <div className="wrapper">
        <Container fluid>
          <Row>
            <Col lg={4} md={6} sm={6} xs={12} className="col-width">
              <Zoom delay={1100}>
                <div className="service-section">
                  <div className="service-heading ">
                    <h1>
                      WEBSITE
                      <br />
                      DEVELOPMENT
                    </h1>
                    <div className="numbers">
                      <h2>01</h2>
                    </div>
                  </div>
                  <div className="service-details">
                    <div className="service-image">
                      <img src={webImg} alt="Box-1" />
                    </div>
                    <div className="service-text-descr">
                      <p>
                        From websites to apps, we develop everything and make it a perfect fit for your business. Manage your business operations, workflow, and satisfy your entire lead with high-end web solutions.
                      </p>
                    </div>
                  </div>
                </div>
              </Zoom>
            </Col>
            <Col lg={4} md={6} sm={6} xs={12} className="col-width">
              <Zoom delay={1300}>
                <div className="service-section box-color">
                  <div className="service-heading ">
                    <h1>
                      MOBILE APP
                      <br />
                      DEVELOPMENT
                    </h1>
                    <div className="numbers">
                      <h2>02</h2>
                    </div>
                  </div>
                  <div className="service-details">
                    <div className="service-image">
                      <img src={mobImg} alt="Box-2" />
                    </div>
                    <div className="service-text-descr">
                      <p>
                        MDSol Tech empowers every app to engage the audience with your business. Boost the trust of your audience with our intuitive mobile apps, and let them be in touch with you.
                      </p>
                    </div>
                  </div>
                </div>
              </Zoom>
            </Col>
            <Col lg={4} md={6} sm={6} xs={12} className="col-width">
              <Zoom delay={1500} duration={1500}>
                <div className="service-section">
                  <div className="service-heading ">
                    <h1>
                      BLOCKCHAIN
                      <br />
                      SERVICES
                    </h1>
                    <div className="numbers numberThree ">
                      <h2>03</h2>
                    </div>
                  </div>
                  <div className="service-details">
                    <div className="service-image">
                      <img src={netImg} alt="Box-3" />
                    </div>
                    <div className="service-text-descr">
                      <p>
                        Blockchain technology is emerging as a business focus for organizations in several industries, including consumer products, manufacturing, financial services, health care, life sciences, and public sector. MDSol helps companies and organizations achieve many goals with respect to blockchain implementation.
                      </p>
                    </div>
                  </div>
                </div>
              </Zoom>
            </Col>
            {/* Second Row Starts */}
            {/* Second Row Starts */}
            <Col lg={4} md={6} sm={6} xs={12} className="col-width">
              <Zoom delay={1700}>
                <div className="service-section box-color">
                  <div className="service-heading ">
                    <h1>UI/UX</h1>
                    <div className="numbers numberFour">
                      <h2>04</h2>
                    </div>
                  </div>
                  <div className="service-details">
                    <div className="service-image">
                      <img src={four} alt="Box-1" />
                    </div>
                    <div className="service-text-descr">
                      <p>
                        User experience or user interaction, we care about both. We know how to attract the audience to your website and mobile app. Our lucrative designs begin the engagement right from the front.
                      </p>
                    </div>
                  </div>
                </div>
              </Zoom>
            </Col>
            <Col lg={4} md={6} sm={6} xs={12} className="col-width">
              <Zoom delay={1900}>
                <div className="service-section">
                  <div className="service-heading ">
                    <h1>
                      DIGITAL
                      <br />
                      MARKETING
                    </h1>
                    <div className="numbers numberFive">
                      <h2>05</h2>
                    </div>
                  </div>
                  <div className="service-details">
                    <div className="service-image">
                      <img src={five} alt="Box-2" />
                    </div>
                    <div className="service-text-descr">
                      <p>
                        Make your brand popular and notable throughout the world with our supreme digital marketing services. Attract the audience across every network and from every demography.
                      </p>
                    </div>
                  </div>
                </div>
              </Zoom>
            </Col>
            <Col lg={4} md={6} sm={6} xs={12} className="col-width">
              <Zoom delay={2100}>
                <div className="service-section box-color">
                  <div className="service-heading ">
                    <h1>
                      SEARCH ENGINE
                      <br />
                      OPTIMIZATION
                    </h1>
                    <div className="numbers numberSix">
                      <h2>06</h2>
                    </div>
                  </div>
                  <div className="service-details">
                    <div className="service-image">
                      <img src={six} alt="Box-3" />
                    </div>
                    <div className="service-text-descr">
                      <p>
                        Earn a natural rank for your website on the search engine in a short time with our expert SEO services. Along with that, retain the top rank and keep on getting the juice of our SEO strategies.
                      </p>
                    </div>
                  </div>
                </div>
              </Zoom>
            </Col>
          </Row>
        </Container>
      </div>
    </div>
  );
};

export default Services;
