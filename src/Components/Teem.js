import React from "react";
import Fade from "react-reveal/Fade";
import Slide from "react-reveal/Slide";

const Teem = () => {
  return (
    <div className="teem-wrapper" id="team">
      <Slide right>
        <div className="main-heading">Meet the team</div>
        <hr />
        <div className="text-para">
          <p className="main-para-head">Meet the team of MDSol experts that are always ready to bring you robust, intuitive, and scalable digital solutions.</p>
        </div>
      </Slide>

      {/* TEEM BOXES Start */}
      {/* TEEM BOXES Start */}

      <div className="box-wrapper">
        {/* first row of boxes */}
        <div className="box-1 image-box spaces"></div>

        <div className="text-box spaces spaces-r">
          <Fade right duration={1500}>
            <div className="title">Zeeshan A.</div>
            <div className="sub-title">project manager</div>
            <div className="about-section">
              <p>
                A passionate behind a realistic dream can turn the impossible into possible. That is what Zeeshan is, a qualified software engineer with a bright idea to serve the business digitally.
              </p>
            </div>
            <div className="svgCon">
              <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                <title>Left, Arrow, Back</title>
                <path d="M12,0A12,12,0,1,0,24,12,12,12,0,0,0,12,0Zm0,22A10,10,0,1,1,22,12,10,10,0,0,1,12,22Z" />
                <polygon points="14.11 5.67 7.77 12 14.11 18.33 15.52 16.92 10.6 12 15.52 7.08 14.11 5.67" />
              </svg>
            </div>
          </Fade>
        </div>
        <div className="parent-box spaces spaces-l">
          <div className="box-3 image-box img-width"> </div>
          <div className="text-box text-box-width">
            <Fade right duration={1500}>
              <div className="title">Nadeem y.</div>
              <div className="sub-title">Lead Software Architect</div>
              <div className="about-section ">
                <p>
                  Inspiration always finds its ways through intuition. Nadeem is a software engineer with the instincts to create robust designs that can maximize the production and revenue for every
                  type of business.
                </p>
              </div>
              <div className="svgCon tab-right">
                <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                  <title>Left, Arrow, Back</title>
                  <path d="M12,0A12,12,0,1,0,24,12,12,12,0,0,0,12,0Zm0,22A10,10,0,1,1,22,12,10,10,0,0,1,12,22Z" />
                  <polygon points="14.11 5.67 7.77 12 14.11 18.33 15.52 16.92 10.6 12 15.52 7.08 14.11 5.67" />
                </svg>
              </div>
            </Fade>
          </div>
        </div>

        {/* first row of boxes End*/}
        {/* second row of boxes start*/}
        <div className="parent-box spaces spaces-r">
          <div className="text-box text-box-width order-text">
            <Fade left duration={1500}>
              <div className="title">SIKANDAR K.</div>
              <div className="sub-title">Director of Business Development</div>
              <div className="about-section">
                <p>
                  You do not have to be just an entrepreneur to develop a business. Instead, you need a scalable and robust mind to discover the ideas to make a business strong and to grow it. That is
                  what Sikandar, is the Director of Business Development.
                </p>
              </div>
              <div className="svgCon right tab-left">
                <svg className="rightSvg" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                  <title>Right, Next, Arrow</title>
                  <path d="M12,0A12,12,0,1,0,24,12,12,12,0,0,0,12,0Zm0,22A10,10,0,1,1,22,12,10,10,0,0,1,12,22Z" />
                  <polygon points="8.48 7.08 13.4 12 8.48 16.92 9.89 18.33 16.23 12 9.89 5.67 8.48 7.08" />
                </svg>
              </div>
            </Fade>
          </div>
          <div className="box-6 image-box img-width order-img"></div>
        </div>
        {/* <span className="mobile-view">
          <div className="text-box order-text-two spaces spaces-l">
            <Fade left duration={1500}>
              <div className="title">SIKANDAR K.</div>
              <div className="sub-title">Full-Stack Developer</div>
              <div className="about-section">
                <p>
                  Understandability comes first in every project. When a team has a software engineer like Sikandar, none of the concepts remains in ambiguity. That is why MDSol can please its every
                  client.
                </p>
              </div>
              <div className="svgCon right">
                <svg className="rightSvg" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                  <title>Right, Next, Arrow</title>
                  <path d="M12,0A12,12,0,1,0,24,12,12,12,0,0,0,12,0Zm0,22A10,10,0,1,1,22,12,10,10,0,0,1,12,22Z" />
                  <polygon points="8.48 7.08 13.4 12 8.48 16.92 9.89 18.33 16.23 12 9.89 5.67 8.48 7.08" />
                </svg>
              </div>
            </Fade>
          </div>

          <div className="box-8 image-box order-img-two spaces"></div>
        </span> */}
        {/* second row of boxes End*/}
        {/* Third row of boxes start*/}
        <div className="box-9 image-box spaces box-9-mview"></div>

        <div className="text-box spaces shehzad-div">
          <Fade right duration={1500}>
            <div className="title">Shehzad S.</div>
            <div className="sub-title">UI/UX Director</div>
            <div className="about-section">
              <p>
                The eye-catching designs and attractive looks on websites and mobile apps engage the audience. Shehzad makes that possible with his UI/UX expertise and vast knowledge in this line.
              </p>
            </div>
            <div className="svgCon right">
              <svg className="rightSvg" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                <title>Right, Next, Arrow</title>
                <path d="M12,0A12,12,0,1,0,24,12,12,12,0,0,0,12,0Zm0,22A10,10,0,1,1,22,12,10,10,0,0,1,12,22Z" />
                <polygon points="8.48 7.08 13.4 12 8.48 16.92 9.89 18.33 16.23 12 9.89 5.67 8.48 7.08" />
              </svg>
            </div>
          </Fade>
        </div>
        <div className="box-9 image-box spaces"></div>

        <div className="parent-box spaces raweewan-div">
          <div className="box-11 image-box img-width"></div>
          <div className="text-box text-box-width">
            <Fade right duration={1500}>
              <div className="title">Raweewan C.</div>
              <div className="sub-title">Director of IT Sales</div>
              <div className="about-section">
                <p>
                  The golden principle of sales is to understand like a customer. When a person can know what his customer will want and what will satisfy his needs, he is capable of being an IT Sales
                  Executive. Raweewan is among those who can empathize with the customers and help them with the best solutions.
                </p>
              </div>
              <div className="svgCon tab-right">
                <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                  <title>Left, Arrow, Back</title>
                  <path d="M12,0A12,12,0,1,0,24,12,12,12,0,0,0,12,0Zm0,22A10,10,0,1,1,22,12,10,10,0,0,1,12,22Z" />
                  <polygon points="14.11 5.67 7.77 12 14.11 18.33 15.52 16.92 10.6 12 15.52 7.08 14.11 5.67" />
                </svg>
              </div>
            </Fade>
          </div>
        </div>
        {/* Third row of boxes End*/}
      </div>
      {/* TEEM BOXES Ends */}
      {/* TEEM BOXES Ends */}
    </div>
  );
};

export default Teem;
