import React from "react";
import Slide from "react-reveal/Slide";

const WebAgency = () => {
  return (
    <div className="web-agency-wrapper">
      <div className="banner-image">
        <div className="text-sec">
          <Slide left>
            <div className="main-heading">
              A Passionate <br /> Web Agency
            </div>
          </Slide>
          <Slide left>
            <div className="sub-heading">
              Intensive for People, <br /> Growth, and Products
            </div>
            <hr />
          </Slide>
          <Slide left>
            <div className="first-para">
              MDSol is here to deliver you the bespoke web and mobile app development services. Our turnkey web solutions and mobile apps can leverage the best in class business experience with utmost performance. We dedicate our efforts to quality and focus on only bringing you the best. Certainly, you get the most out of the online venture.
            </div>
            <div className="second-para">
              Take this into practicality with the words of our clients whom we have been serving with the finest digital presence products. The online presence of your business can lead you to not just double, or triple, indeed ten times higher ROI.
              So let us begin today, to strengthen your business right away!
            </div>
          </Slide>
        </div>
      </div>
    </div>
  );
};

export default WebAgency;
