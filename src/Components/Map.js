import React from "react";
import logo from "../assets/images/logo.png";

const Map = () => {
  return (
    <div className="map-section-wrapper" id="contact">
      <div className="flex-container">
        <div className="text-section">
          <div className="logo">
            <img src={logo} alt="" className="img-fluid" />
          </div>
          <div className="text-para">
            As a full-service software development company, MDSOL offers a range
            of technology services spanning in most demanded types of
            Technology.
          </div>
          {/* <div className="phone mt-3">
            <span>Phone</span>
            <a href="tel:+92 317 7675689">+92 317 7675689</a>
          </div> */}
          <div className="email">
            <span>Email</span>
            <a href="mailto: info@mdsoltech.com">info@mdsoltech.com</a>
          </div>
          <div className="access">
            <span>Address</span>
            <p>J Block Johar Town, Lahore</p>
          </div>
        </div>
        <div className="map-section">
          <iframe title="Office Location" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3402.8926725547835!2d74.25502536462963!3d31.472138456672294!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3919022e5caf774f%3A0x75bd2de2b652bbf1!2sBlock%20J%20Phase%202%20Johar%20Town%2C%20Lahore%2C%20Punjab%2C%20Pakistan!5e0!3m2!1sen!2s!4v1636538882133!5m2!1sen!2s" loading="lazy"></iframe>
        </div>
      </div>
    </div>
  );
};

export default Map;
